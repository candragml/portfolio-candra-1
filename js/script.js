
window.addEventListener("load", ()=>{
    document.querySelector(".main").classList.remove("hidden");
    document.querySelector(".home-section").classList.add("active");

    document.querySelector(".page-loader").classList.add("fade-out");
    setTimeout(() => {
        document.querySelector(".page-loader").style.display = "none";
        
    }, 600);
});

// /* =================toggle navbar================= */ 
const navToggler = document.querySelector(".nav-toggler");
      navToggler.addEventListener("click", () =>{
        hideSection();
        toggleNavbar();
        document.body.classList.toggle("hide-scrolling");
      });

function hideSection() {
    document.querySelector("section.active"). classList.toggle("fade-out");
}

function toggleNavbar() {
    document.querySelector(".header").classList.toggle("active");
}
// /* =================Active Section================= */ 
document.addEventListener("click", (e) =>{
        if(e.target.classList.contains("link-item") && e.target.hash !== ""){
            
            document.querySelector(".overlay").classList.add("active");
            navToggler.classList.add("hide");
        if(e.target.classList.contains("nav-item")){
            toggleNavbar();          
        }
        else{
            hideSection();
            document.body.classList.add("hide-scrolling");
        }
        setTimeout(() =>{
            document.querySelector("section.active").classList.remove("active", "fade-out");
            document.querySelector(e.target.hash).classList.add("active");
            window.scrollTo(0,0);
            document.body.classList.remove("hide-scrolling");
            navToggler.classList.remove("hide");
            document.querySelector(".overlay").classList.remove("active");
        }, 500);
    }
});
// /* =================About Tabs================= */ 
const tabsContainer = document.querySelector(".about-tabs"),
      aboutSection = document.querySelector(".about-section");
      
      tabsContainer.addEventListener("click", (e) =>{
        if(e.target.classList.contains("tab-item") && !e.target.classList.contains("active")){
            tabsContainer.querySelector(".active").classList.remove("active");
            e.target.classList.add("active");
            const target = e.target.getAttribute("data-target");
            aboutSection.querySelector(".tab-content.active").classList.remove("active");
            aboutSection.querySelector(target).classList.add("active"); 
        }
    });


//* ===============================Portfolio filter dan popup======================================*//
	function bodyScrollingToggle(){
		document.body.classList.toggle("stop-scrolling");
	}
	const filterContainer = document.querySelector(".portfolio-filter"),
	portfolioItemsContainer = document.querySelector(".portfolio-items"),
	portfolioItems = document.querySelectorAll(".portfolio-item"),
	popup = document.querySelector(".portfolio-popup"),
	prevBtn = popup.querySelector(".pp-prev"),
	nextBtn = popup.querySelector(".pp-next"),
	closeBtn = popup.querySelector(".pp-close"),
	projectDetailsContainer = popup.querySelector(".pp-details"),
	projectDetailsBtn = popup.querySelector(".pp-project-details-btn");
	let itemIndex, slideIndex, screenshots;

	// Portfolio Filter Items
	filterContainer.addEventListener("click", (e) =>{
		if(e.target.classList.contains("filter-item") &&
			!e.target.classList.contains("active")){

			// Aktivasi eksis aktif 'filter-item'
			filterContainer.querySelector(".active").classList.remove( "active");
			// Aktivasi new 'filter-item'
			e.target.classList.add("active");
			const target = e.target.getAttribute("data-target");
			
			portfolioItems.forEach((item) =>{
			// console.log(item.getAttribute("data-category"));
			if(target === item.getAttribute("data-category") || target === 'all'){
				item.classList.remove("hide");
				item.classList.add("show");
			}else{
				item.classList.remove("show");
				item.classList.add("hide");
			}
		  })
		}
	})

	portfolioItemsContainer.addEventListener("click", (e) =>{
		if(e.target.closest(".portfolio-item-inner")){
			const portfolioItem = e.target.closest(".portfolio-item-inner").
				parentElement;
			// Get Portfolio Index
			itemIndex = Array.from(portfolioItem.parentElement.children).indexOf(portfolioItem);
			// console.log(itemIndex);
			screenshots = portfolioItems[itemIndex].querySelector(".portfolio-item-img img").getAttribute("data-screenshots");
			// Convert Screenshot Into Array
			screenshots = screenshots.split(",");
			if(screenshots.length === 1){
				prevBtn.style.display="none";
				nextBtn.style.display="none";
			}else{
				prevBtn.style.display="block";
				nextBtn.style.display="block";
			}
			// console.log(screenshots)
			slideIndex = 0;
			popupToggle();
			popupSlideshow();
			popupDetails();
		}
	})

	closeBtn.addEventListener("click", () =>{
		popupToggle();
		if(projectDetailsContainer.classList.contains("active")){
			popupDetailsToggle();
		}
	})

	function popupToggle(){
		popup.classList.toggle("open");
		bodyScrollingToggle();
	}

	function popupSlideshow(){
		const imgSrc = screenshots[slideIndex];
		const popupImg = popup.querySelector(".pp-img");
		// Aktif Loader until popup img loaded
		popup.querySelector(".pp-loader").classList.add("active");
		popupImg.src=imgSrc;
		popupImg.onload = () =>{
			// 
			popup.querySelector(".pp-loader").classList.remove("active");
		}
			popup.querySelector(".pp-counter"). innerHTML = (slideIndex+1) + " - " +
			screenshots.length;
	}

	// nextSlide
	nextBtn.addEventListener("click", ()=>{
		if(slideIndex === screenshots.length-1){
			slideIndex = 0;
		}else{
			slideIndex++;
		}
		popupSlideshow();
	})

	// prevSlide
	prevBtn.addEventListener("click", ()=>{
		if(slideIndex === 0){
			slideIndex = screenshots.length-1
		}else{
			slideIndex--;
		}
		popupSlideshow();
	})

	function popupDetails(){
		if(!portfolioItems[itemIndex].querySelector(".portfolio-item-details")){
			projectDetailsBtn.style.display = "none";
			return; //end function execute
		}
		projectDetailsBtn.style.display = "block";
		// get the proojec
		const details = portfolioItems[itemIndex].querySelector(".portfolio-item-details").innerHTML;
		// get the proojec
			popup.querySelector(".pp-project-details").innerHTML = details;
		// get the project title
		const title = portfolioItems[itemIndex].querySelector(".portfolio-item-title").innerHTML;
		// set project title
			popup.querySelector(".pp-title h2").innerHTML = title;	
		// get project kategori
		const category = portfolioItems[itemIndex].getAttribute("data-category");
		// set project kategori
		popup.querySelector(".pp-project-category").innerHTML = category.split("-").join(" ");
	}

	projectDetailsBtn.addEventListener("click", ()=>{
		popupDetailsToggle();
	})

	function popupDetailsToggle() {
		if(projectDetailsContainer.classList.contains("active")){
			projectDetailsBtn.querySelector("i").classList.remove("fa-minus");
			projectDetailsBtn.querySelector("i").classList.add("fa-plus");
			projectDetailsContainer.classList.remove("active");
			projectDetailsContainer.style.maxHeight = 0 + "px"; 
		}else{
			projectDetailsBtn.querySelector("i").classList.remove("fa-plus");
			projectDetailsBtn.querySelector("i").classList.add("fa-minus");
			projectDetailsContainer.classList.add("active");
			projectDetailsContainer.style.maxHeight = projectDetailsContainer.
				scrollHeight + "px";
			popup.scrollTo(0,projectDetailsContainer.offsiteTop);
		}
	};
